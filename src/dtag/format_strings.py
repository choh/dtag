"""Functions for formatting strings/artist names."""
import re
from typing import Iterable, Dict, Any, List

def format_artist(artist_data: Iterable[dict], use_anv: bool = True) -> str:
    """Format the 'artists' data returned by the discogs API.

    Args:
        artist_data: The artist data taken from the release info JSON.
        use_anv: Whether to use ANV if present. Default is true.

    The data is a dictionary containing at least the name of the artist
    of the release, but depending on the release you might need to format
    extra_artists for individual tracks as well.

    Return: A string giving the formatted artist name.
    """
    artist_list: List[List[str]] = []

    artist: Dict[str, Any]
    for artist in artist_data:
        if use_anv and "anv" in artist and len(artist["anv"]) > 0:
            artist_key = "anv"
        else:
            artist_key = "name"

        artist_name = remove_enumeration(artist[artist_key])
        artist_join = " {}".format(artist["join"]) \
            if artist["join"] not in (",", "") \
            else artist["join"]
        artist_list.append([artist_name, artist_join])

    # grow artist_str so we can later assign by index
    artist_str: List[str] = [""] * len(artist_list)

    for i, _ in enumerate(artist_list):
        if i < len(artist_list) - 1:
            artist_list[i][1] = artist_list[i][1] \
                if artist_list[i][1] != "" \
                else ","

        artist_str[i] = "".join(artist_list[i])

    return " ".join(artist_str)


def remove_enumeration(artist_name: str) -> str:
    """Remove enumeration in artist names.

    The Discogs database enumerates artists with identical names to
    differentiate between them, so there might be Artist, Artist (2),
    Artist (3) and so on. This function removes that enumeration from
    the artist names.

    Args:
        artist_name: The name of an artist.

    Return:
        The artist name with the enumeration removed.
    """
    artist = re.sub(r"\s+\(\d+\)$", "", artist_name)
    return artist
