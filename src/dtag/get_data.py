"""High-level wrapper for a typical/intended workflow. For regular use you
probably won't need to dive deeper into the tool."""

from typing import Union, Tuple

from .api import fetch_release_data
from .extract import extract_release_data, get_json
from .tracklist import handle_tracklist

def get_data(release: Union[int, str]) -> Tuple[dict, list]:
    """A wrapper function for fetching and formatting data.

    This function takes care of all the data fetching, formatting and
    cleaning that is necessary.

    Args:
        release: A release id either as string or as integer.
    """
    try:
        release_id = str(int(release))
    except ValueError:
        try:
            url_parts = str(release).split("/")
            release_id = str(int(url_parts[-1]))
        except Exception as excpt:  # pylint: disable=broad-except
            raise RuntimeError("Can't extract release ID.") from excpt

    release_data, weburl = fetch_release_data(release_id)
    release_json  = get_json(release_data)
    album_info = extract_release_data(release_json)
    album_info["comment"] = "Data provided by Discogs. URL: {}".format(weburl)
    tracks = handle_tracklist(release_json["tracklist"])

    for track in tracks:
        if track["artist"] is None:
            track["artist"] = album_info["albumartist"]

    return album_info, tracks
