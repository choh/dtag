"""Command line entry point."""

import argparse
from typing import Sequence, Tuple, Union
from .set_data import set_data

def cli() -> None:
    """Command line entry point."""
    in_files, release = arguments()
    set_data(release, in_files)


def arguments(argv: Union[Sequence, None]=None) -> Tuple[Sequence[str], str]:
    """Parse command line arguments.

    Args:
        argv: Optional list of command line arguments. If unspecified
            sys.argv is used.

    Return:
        A tuple with the input files as first item and the release id as
        second item.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input",
        help="Files to tag. Files should be specified in the order they "
             "appear on the release.",
        nargs="+",
        type=str,
        required=True
    )
    parser.add_argument(
        "-r", "--release",
        help="The relase ID or URL to use. This can either be the actual "
             "release ID or the URL to the release page.",
        type=str,
        required=True
    )
    args = parser.parse_args(argv)
    return args.input, args.release
