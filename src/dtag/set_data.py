"""High-level wrapper for writing data to file."""

import re
from typing import Sequence, Union
import magic # type: ignore
from .get_data import get_data
from .set_tag import write_flac_tags, write_mp3_tags

def set_data(release: Union[int, str], files: Sequence[str]) -> None:
    """Write tags to files.

    Args:
        release: The release ID or Discogs URL to use.
        files: The files to write to. Can be FLAC or MP3.
    """
    try:
        release = str(int(release))
    except ValueError:
        release = extract_release_from_url(str(release))


    album_data, track_data = get_data(release)
    if len(track_data) != len(files):
        raise RuntimeError("Mismatch between amount of files and amount "
                           "of tracks in release data.")

    for idx, track in enumerate(files):
        this_data = {**album_data, **track_data[idx]}
        file_type = magic.from_file(track)
        if "FLAC audio" in file_type:
            write_flac_tags(this_data, track)
        elif "MPEG" in file_type and "layer III" in file_type:
            write_mp3_tags(this_data, track)
        else:
            raise RuntimeError("Unknown file type.")


def extract_release_from_url(url: str) -> str:
    """Take a URL to discogs and get the release ID.

    Args:
        url: The URL to parse.

    Return:
        A release ID.
    """
    if "discogs" not in url.lower():
        raise ValueError("Not a Discogs URL")

    if "release" not in url.lower():
        raise ValueError

    pattern = r"^.+/(\d+)$"
    match = re.search(pattern, url)
    if match:
        release_id = match.group(1)
    else:
        raise ValueError("Can't extract release id from url.")

    try:
        int(release_id)
    except ValueError as error:
        raise ValueError("Release ID seems to be invalid, "
                         "must be numbers only.") from error

    return release_id
