"""Error handling wrapper/decorator."""
# pylint: disable=inconsistent-return-statements
from typing import TypeVar, Any, Callable, cast
import requests

FuncType = TypeVar("FuncType", bound=Callable[..., Any])

def request_exception_handling(function: FuncType) -> FuncType:
    """Handle errors.

    This function is primarily meant to be used as decorator to enable
    exception handling for exceptions thrown by the requests package.

    Args:
        function: A function making use of requests to handle errors for.

    Return: The supplied function wrapped with some error handling for
    the requests library.
    """
    def handle_func(*args, **kwargs):
        for i in range(2):
            try:
                return function(*args, **kwargs)
            except requests.Timeout:
                print("Connection timed out")
            except requests.URLRequired:
                print("Invalid URL")
                break
            except requests.ConnectionError:
                print("Can't connect")
            if i == 0:
                print("Retrying...")

    return cast(FuncType, handle_func)
