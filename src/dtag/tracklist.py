"""Tools to handle tracklists. Tracklists have many special cases we try
our best to address them here."""

from typing import List, Dict, Any

from .format_strings import format_artist

def handle_tracklist(tracklist: List[dict]) -> List[Dict[str, Any]]:
    """Extract data for individual tracks.

    Args:
        tracklist: The raw tracklist from the API's response.

    Return:
        A list containing one dict with the relevant data for each track.
        The list is in order.
    """
    track_order = order_tracks(tracklist)

    tracks: List[Dict[str, Any]] = [dict() for _ in range(len(tracklist))]
    for item in tracklist:
        track_num = track_order[item["position"]]
        tracks[track_num] = {
            "tracknumber": track_num + 1,
            "title": add_extra_artists(item),
            "artist": format_artist(item["artists"]) if "artists" in item else None
        }

    return tracks


def order_tracks(tracklist: List[dict], start_at_one: bool = False) -> dict:
    """Take a tracklist and find the correct order.

    This is trivial for music released digitally or on CD, but formats
    like records and cassetted often use a combination of letters and
    numbers like AA, B2, C. The latter needs to be _translated_ into
    numbers for music playing software.

    Args:
        tracklist: The raw tracklist from the API's response.
        start_at_one: Specify whether track numbering should start at 0
            (the default) or at 1

    Return:
        A dict containing an association between _raw_ track numbers and
        their numeric ordering.
    """
    shift = 1 if start_at_one else 0
    track_pos = [item["position"] for item in tracklist]
    track_order = {}
    for num, pos in enumerate(sorted(track_pos)):
        track_order[pos] = num + shift

    return track_order


def add_extra_artists(track_data: Dict[str, Any], force: bool = False,
                      use_anv: bool = True) -> str:
    """Apply extra artist information to title.

    This takes extra artist information such as featuring or remix
    credits and tries to apply it to the title in a sensible way.

    By default this function does only handle featuring and remix credits
    and everything else is dropped. If you want to apply all extra artist
    information you need to set force to True, but then there is no guarantee
    that the result will look nice.

    Args:
        track_data: A dictionary containing information for one track.
        force: If True apply all extra credits, if False only those where
            specific formatting has been taken care of.
        use_anv: If True use artist variation as it appears on the release.

    Return:
        A string of the formatted track title.
    """
    if "extraartists" not in track_data:
        return track_data["title"]

    roles: Dict[str, Any] = {}
    for artist in track_data["extraartists"]:
        this_role = artist["role"]
        if this_role in roles:
            roles[this_role].append(artist)
        else:
            roles[this_role] = [artist]

    formatted_roles = {}
    for role_key, role_value in roles.items():
        formatted_roles[role_key] = format_artist(role_value, use_anv=use_anv)

    formatted_title = track_data["title"]

    if "Featuring" in formatted_roles:
        formatted_title = "{} feat. {}" \
            .format(formatted_title, formatted_roles["Featuring"])

    if "Remix" in formatted_roles and \
        formatted_roles["Remix"] not in formatted_title:
        formatted_title = "{} ({} Remix)" \
            .format(formatted_title, formatted_roles["Remix"])

    if force:
        formatted_roles.pop("Featuring", None)
        formatted_roles.pop("Remix", None)
        for role_key, role_value in formatted_roles.items():
            formatted_title = "{} {} {}" \
                .format(formatted_title, role_key, role_value)

    return formatted_title
