"""Return subsets of unprocessed data."""

from typing import Dict, Any, List

import requests

from .format_strings import format_artist
from .tracklist import handle_tracklist

def get_json(release_data: requests.Response) -> dict:
    """Take an API response and tranform it to JSON.

    Raises a RuntimeError if the server responded with something different
    than HTTP status code 200.

    Args:
        release_data: A raw requests.Response as returned from an API call.

    Return: A dict containing the response body.
    """
    if release_data.status_code != 200:
        raise RuntimeError("Error in API response.")

    release_json = release_data.json()
    return release_json


def extract_release_data(release_data: Dict[str, Any]) -> dict:
    """Extract information needed for tagging.

    This returns data that is needed for all files in a release, but
    does not work on the tracklist.

    Args:
        release_data: The result of a call to the Discogs API.

    Return:
        A dictionary of the data needed for tagging.
    """
    needed_fields = ["artists", "year", "title"]
    for key in needed_fields:
        if key not in release_data.keys():
            raise RuntimeError("Release information incomplete.")

    albumartist = format_artist(release_data["artists"])

    return {
        "albumartist": albumartist,
        "album": release_data["title"],
        "date": release_data["year"]
    }


def extract_tracklist(release_data: requests.Response) -> List[Dict[str, Any]]:
    """Get tracklist data.

    From a raw response object from a call to the Discogs API, extract
    the tracklist data.

    Args:
        release_data: A raw API response for release data.

    Returns: A dict containing the tracklist of the release.
    """
    release_json = get_json(release_data)
    tracks = handle_tracklist(release_json["tracklist"])
    return tracks
