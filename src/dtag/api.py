"""API interaction, mainly fetching release data."""
from typing import Tuple, Union
import requests
from .error_handling import request_exception_handling

@request_exception_handling
def fetch_release_data(release_id: str) \
    -> Tuple[requests.Response, Union[str, None]]:
    """Fetch data for one specified release.

    Use this if you know which release to fetch data for.

    Args:
        release_id: (str) ID of the release to fetch.

    Return: A tuple with the raw requests.Response object returned form the
    API call as well as the URL where the release can be accessed on the web.
    """
    base_url = "https://api.discogs.com/releases/"
    call_url = base_url + release_id

    headers = {
        "User-Agent": "dtag/0.1"
    }

    res = requests.get(call_url, headers=headers)
    web_url = None if res.status_code != 200 else call_url.replace("api.", "")
    return res, web_url
