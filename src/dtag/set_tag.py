"""Functions to write metadata to files."""
from typing import Dict, Union
from mutagen import flac, easyid3 # type: ignore

def write_flac_tags(release_data: Dict[str, Union[str, int]],
                    filename: str) -> None:
    """Write tags to FLAC file.

    Args:
        release_data: A single dictionary containing all data to write to
            the file.
        filename: Path to the file to work on.
    """
    audio_data = flac.FLAC(filename)
    for key in release_data:
        audio_data.tags[key.upper()] = str(release_data[key])
    audio_data.save()


def write_mp3_tags(release_data: Dict[str, Union[str, int]],
                   filename: str) -> None:
    """Write tags to MP3 file.

    Args:
        release_data: A single dictionary containing all data to write to
            the file.
        filename: Path to the file to work on.
    """
    easyid3.EasyID3.RegisterTextKey("comment", "COMM")
    metadata = easyid3.EasyID3()
    for key in release_data:
        metadata[key.upper()] = str(release_data[key])
    # tags with v2_version=4 (the default) seem to cause trouble for some
    # applications
    metadata.save(filename, v2_version=3)
