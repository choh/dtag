# pylint: disable=missing-module-docstring, missing-function-docstring
import nox

@nox.session(python="3.7", reuse_venv=True)
def lint(session):
    session.install(".")
    tolint = "src", "tests", "noxfile.py"
    session.install("pylint", "nox", "pytest")
    session.run("pylint", *tolint)

@nox.session(python="3.7", reuse_venv=True)
def typing(session):
    session.install(".")
    session.install("mypy", "types-requests")
    session.run("mypy", "src/")

@nox.session(python="3.7", reuse_venv=True)
def docs(session):
    session.install(".")
    session.install("sphinx")
    session.run("make", "-C", "docs", "html", external=True)

@nox.session(python="3.7", reuse_venv=True)
def tests(session):
    session.install(".")
    session.install("pytest", "pytest-cov")
    session.run("pytest")
