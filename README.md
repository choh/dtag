# `dtag`

The `dtag` package provides a simple tool to tag audio files using 
data available from the Discogs website. 
The main motivation to write this tool was to quickly tag files obtained 
from digitalising vinyl records, but maybe someone else finds this useful as 
well.

After installation there is a `dtag` command available. Use `dtag -h` for 
an overview of its – relatively simple – usage.

---

This application uses Discogs’ API but is not affiliated with, sponsored or 
endorsed by Discogs. ‘Discogs’ is a trademark of Zink Media, LLC.