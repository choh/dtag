# pylint: disable=missing-function-docstring, missing-module-docstring
import pytest
from dtag import get_data

def test_get_data_of_invalid_release():
    invalid_release = "abc"

    with pytest.raises(RuntimeError) as exc:
        get_data.get_data(invalid_release)

    assert "Can't extract release ID." in str(exc.value)


def test_get_data_of_nonexisting_release():
    non_exiting_release = 999999999999999999999

    with pytest.raises(RuntimeError) as exc:
        get_data.get_data(non_exiting_release)

    assert "Error in API response." in str(exc.value)


def test_get_data_of_existing_release():
    release = 12223741
    data = get_data.get_data(release)
    assert len(data) == 2
    assert isinstance(data, tuple)

    assert isinstance(data[0], dict)
    album_fields = ["album", "albumartist", "date", "comment"]
    assert {x in data[0] for x in album_fields}.pop()

    assert {isinstance(x, dict) for x in data[1]}.pop()
    track_fields = ["tracknumber", "title", "artist"]
    assert {x in track for x in track_fields for track in data[1]}.pop()
