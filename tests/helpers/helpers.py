import os
import shutil

def copy_files(list_of_files, prefix="testcopy", remove_existing=True):
    copies = []
    for this_file in list_of_files:
        path, file_name = os.path.split(this_file)
        file_name = "{}_{}".format(prefix, file_name)
        new_path = os.path.join(path, file_name)
        if os.path.exists(new_path):
            if remove_existing:
                os.remove(new_path)
            else:
                raise FileExistsError("File {} already exists." \
                    .format(new_path))

        shutil.copy(this_file, new_path)
        copies.append(new_path)
    return copies
