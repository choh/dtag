# pylint: disable=missing-function-docstring, missing-module-docstring
import os

from mutagen import flac, easyid3
from dtag import set_tag
from .helpers.helpers import copy_files



def test_set_flac_tag(dummy_release_data):
    dir_content = list(os.scandir("tests/testfiles/release_1"))
    target_files = [x for x in dir_content if "/track" in x.path]
    use_files = copy_files(target_files)
    album, tracks = dummy_release_data
    tag_list = []
    for idx, this_file in enumerate(use_files):
        release_data = {**album, **tracks[idx]}
        set_tag.write_flac_tags(release_data, this_file)
        tag_list.append(flac.FLAC(this_file))

    assert tag_list[0]["title"][0] == tracks[0]["title"]
    assert tag_list[1]["artist"][0] == tracks[1]["artist"]
    assert tag_list[2]["album"][0] == album["album"]


def test_set_mp3_tag(dummy_release_data):
    dir_content = list(os.scandir("tests/testfiles/release_2"))
    target_files = [x for x in dir_content if "/track" in x.path]
    use_files = copy_files(target_files)
    album, tracks = dummy_release_data
    tag_list = []
    for idx, this_file in enumerate(use_files):
        release_data = {**album, **tracks[idx]}
        set_tag.write_mp3_tags(release_data, this_file)
        tag_list.append(easyid3.EasyID3(this_file))

    assert tag_list[0]["title"][0] == tracks[0]["title"]
    assert tag_list[1]["artist"][0] == tracks[1]["artist"]
    assert tag_list[2]["album"][0] == album["album"]
