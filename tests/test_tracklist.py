# pylint: disable=missing-function-docstring, missing-module-docstring
from dtag import tracklist

def order_helper(ordered_tracklist):
    ordered_keys = [None] * len(ordered_tracklist)
    for key in ordered_tracklist:
        ordered_keys[ordered_tracklist[key]] = key
    return ordered_keys

def test_handle_tracklist_includes_necessary_fields(dummy_tracklist):
    formatted_tracks = tracklist.handle_tracklist(dummy_tracklist)
    track_fields = ["tracknumber", "title", "artist"]
    assert {field in track
        for field in track_fields
        for track in formatted_tracks}.pop()
    assert len(formatted_tracks) == len(dummy_tracklist)

def test_order_tracks_numeric():
    this_tracklist = []
    for i in range(1, 6):
        this_tracklist.append({ "position": str(i) })

    ordered = tracklist.order_tracks(this_tracklist)
    assert [t["position"] for t in this_tracklist] == order_helper(ordered)

def test_order_tracks_vinyl(dummy_tracklist):
    ordered = tracklist.order_tracks(dummy_tracklist)
    assert ["A1", "B1", "B2"] == order_helper(ordered)

def test_add_extra_artists_returns_title_if_no_extraartist(dummy_tracklist):
    no_extra_artist = tracklist.add_extra_artists(dummy_tracklist[2])
    assert no_extra_artist == dummy_tracklist[2]["title"]

def test_add_extra_artist_appends_remix_in_parentheses(dummy_tracklist):
    remix_track = tracklist.add_extra_artists(dummy_tracklist[1])
    extra_name = dummy_tracklist[1]["extraartists"][0]["name"]
    assert remix_track == "{} ({} Remix)".format(
        dummy_tracklist[1]["title"], extra_name)

def test_add_extra_artist_append_featuring(dummy_tracklist):
    featuring_track = tracklist.add_extra_artists(dummy_tracklist[0])
    extra_name = dummy_tracklist[0]["extraartists"][0]["name"]
    assert featuring_track == "{} feat. {}".format(
        dummy_tracklist[0]["title"], extra_name)
