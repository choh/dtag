# pylint: disable=missing-function-docstring, missing-module-docstring
import pytest
from dtag import set_data

def extract_release_from_url_valid_url_with_name():
    this_url = "https://www.discogs.com/Iri-Sparkle/release/15405405"
    release_id = set_data.extract_release_from_url(this_url)
    assert release_id == "15405405"

def extract_release_from_url_valid_url_without_name():
    this_url = "https://www.discogs.com/release/15405405"
    release_id = set_data.extract_release_from_url(this_url)
    assert release_id == "15405405"

def extract_release_from_url_invalid_master_release_url():
    this_url = "https://www.discogs.com/master/1749054"
    with pytest.raises(ValueError):
        set_data.extract_release_from_url(this_url)

def extract_release_from_url_invalid_url_from_other_site():
    this_url = "https://news.ycombinator.com/"
    with pytest.raises(ValueError) as exc:
        set_data.extract_release_from_url(this_url)
    assert "Not a Discogs URL" in str(exc.value)
