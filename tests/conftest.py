# pylint: disable=missing-function-docstring, missing-module-docstring

import pytest

@pytest.fixture
def dummy_artist():
    yield [{
        'name': 'J. Random Artist (17)',
        'anv': 'Test Artist',
        'join': '',
        'role': '',
        'tracks': '',
        'id': 000000,
        'resource_url': ''
    }]

# that is not pretty but it gets the job done of making an independent
# copy of the data available if needed
second_dummy_artist = dummy_artist

@pytest.fixture
def dummy_tracklist():
    return [{
        'position': 'A1',
        'type_': 'track',
        'title': 'Test Track',
        'extraartists': [{
            'name': 'DJ Test',
            'anv': '',
            'join': '',
            'role': 'Featuring',
            'tracks': '',
            'id': 0,
            'resource_url': ''
        }],
        'duration': ''
    }, {
        'position': 'B2',
        'type_': 'track',
        'title': 'Test 2021',
        'extraartists': [{
            'name': 'Pytest',
            'anv': '',
            'join': '',
            'role': 'Remix',
            'tracks': '',
            'id': 0,
            'resource_url': ''
        }],
        'duration': ''
    }, {
        'position': 'B1',
        'type_': 'track',
        'title': 'TestTest',
        'duration': ''
    }]

@pytest.fixture
def dummy_release_data():
    album = {
        'albumartist': 'DJ Python',
        'album': 'Testing is Fun',
        'date': 2016,
        'comment': 'Testdata'
    }

    tracks = [
        {
            'tracknumber': 1,
            'title': 'Python feat. Snakes',
            'artist': 'DJ Python'
        },
        {
            'tracknumber': 2,
            'title': 'Boa',
            'artist': 'DJ Python'
        },
        {
            'tracknumber': 3,
            'title': 'Python Instrumental',
            'artist': 'DJ Python'
        }
    ]

    return album, tracks
