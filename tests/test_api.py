# pylint: disable=missing-function-docstring, missing-module-docstring
import pytest
import requests
from dtag import api

def test_fetch_release_data_of_existing_release():
    release_id = "9110682"
    resp, url = api.fetch_release_data(release_id)

    assert isinstance(resp, requests.Response)
    assert resp.status_code == 200
    assert "discogs" in url


def test_fetch_release_data_with_incorrect_type():
    release_id = 9110682

    with pytest.raises(TypeError) as exc:
        api.fetch_release_data(release_id)

    assert "an only concatenate str (not \"int\") to str" in str(exc.value)


def test_fetch_release_data_of_nonexisting_release():
    release_id = "99999999999999999"
    resp, url = api.fetch_release_data(release_id)

    assert isinstance(resp, requests.Response)
    assert resp.status_code == 404
    assert url is None


def test_fetch_release_data_with_invalid_release_id():
    release_id = "testtest"
    resp, url = api.fetch_release_data(release_id)

    assert isinstance(resp, requests.Response)
    assert resp.status_code == 404
    assert url is None
