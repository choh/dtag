# pylint: disable=missing-function-docstring, missing-module-docstring
from dtag import format_strings

def test_format_artist_with_anv(dummy_artist):
    formatted = format_strings.format_artist(dummy_artist, use_anv=True)
    assert isinstance(formatted, str)
    assert formatted == dummy_artist[0]["anv"]

def test_format_artist_without_anv(dummy_artist):
    formatted = format_strings.format_artist(dummy_artist, use_anv=False)
    assert isinstance(formatted, str)
    assert formatted == "J. Random Artist"

def test_format_artist_two_with_join(dummy_artist, second_dummy_artist):
    add_artist = second_dummy_artist
    dummy_artist[0]["join"] = "&"
    dummy_artist.append(add_artist[0])
    formatted = format_strings.format_artist(dummy_artist)
    assert isinstance(formatted, str)
    assert formatted == "Test Artist & Test Artist"

def test_format_artist_two_without_join(dummy_artist):
    dummy_artist.append(dummy_artist[0])
    formatted = format_strings.format_artist(dummy_artist)
    assert isinstance(formatted, str)
    assert formatted == "Test Artist, Test Artist"

def test_remove_enumeration_in_name_without_enumeration():
    name = "Artist Name"
    assert format_strings.remove_enumeration(name) == name

def test_remove_enumeration_in_name_with_enumeration():
    name = "Artist Name (24)"
    assert format_strings.remove_enumeration(name) == "Artist Name"

def test_remove_enumeration_with_enumeration_like_pattern_inline():
    name = "Artist Name (2) Name"
    assert format_strings.remove_enumeration(name) == name

def test_remove_enumeration_with_enumeration_like_at_end_without_space():
    name = "Artist(10)"
    assert format_strings.remove_enumeration(name) == name
